
const { MongoClient } = require('mongodb');

let mongodb = null;
let mongodbStaging = null;

// URL PRODUCTION mongodb+srv://mycimencam:ikryx7BpkNifHNjQ@cluster0-mnzqj.gcp.mongodb.net/mycimencam?retryWrites=true
// URL STAGING mongodb+srv://mycimencam:ohg3YmGw2agRNpUh@cluster0-gt1w5.gcp.mongodb.net/mycimencam?retryWrites=true
// URL DEV mongodb://127.0.0.1:27017/mycimencam

async function getMongoDB() {
    if (mongodb) { return mongodb; }

    const mongoDBURL = `mongodb+srv://mycimencam:ikryx7BpkNifHNjQ@cluster0-mnzqj.gcp.mongodb.net/mycimencam?retryWrites=true`;
    const connection = await MongoClient.connect(mongoDBURL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    mongodb = connection.db();
    return mongodb;
}

module.exports = { getMongoDB };