const { getMongoDBStaging, getMongoDBDev } = require('./config');

// index.js
const cron = require("node-cron");
const express = require("express");
const http = require('request-promise');
const moment = require('moment');
const { MongoClient } = require('mongodb');

app = express();

// schedule tasks to be run on the server

cron.schedule("0 */1 * * *", async () => {
    console.log("running a task every 1 hour");

    let currentDate = moment();
    let dateToSend = currentDate.format('YYYY/MM/DD');
    let timeToSend = currentDate.format('HHmmss');
    let dateFrom = currentDate.subtract(1, 'hours');
    let dateFromSend = dateFrom.format('YYYY/MM/DD');
    let timeFromSend = dateFrom.format('HHmmss');

    let retreivements;

    try {
        // Remove certificate verification
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = 0;

        const options = {
            method: 'GET',
            uri: `https://mycimencam.com/api/v1/jde-dispatch-load`,
            qs: {
                datefrom: dateFromSend,
                dateTo: dateToSend,
                timeFrom: timeFromSend,
                timeTo: timeToSend
            }
        };
        const result = await http(options);
        console.log('Request options : ', options);
        const jdeResponse = JSON.parse(result).cpLifeDispatchDetailsReply;

        // Add certificate verification
        process.env.NODE_TLS_REJECT_UNAUTHORIZED = 1;

        retreivements = jdeResponse.map(element => {
            let retreiveElt = {
                driverName: element.Header.DriverName,
                registrationLicenseNumber: element.Header.RegistrationLicenseNumber,
                primaryVehicleId: element.Header.PrimaryVehicleId,
                shipTo: parseInt(element.Header.ShipTo),
                soldTo: parseInt(element.Header.SoldTo),
                salesOrderNumber: parseInt(element.Header.SalesOrderNumber),
                shipmentNumber: parseInt(element.Header.ShipmentNumber),
                loadNumber: parseInt(element.Header.LoadNumber),
                actualShipDate: moment(element.Header.ActualShipDate).valueOf(),
                actualShipTime: element.Header.ActualShipTime,
                lifeTicketNumber: parseInt(element.Header.LIFETicketNumber),
                details: getRetreivementItems(element.Details)
            };
            return retreiveElt;
        });

        // Insert new retreivements in DB

        const db = await getMongoDB();

        console.log('----------------------------------------');
        console.log('INSERT NEW Retreivements');
        console.log('----------------------------------------');

        let retreivementsToInsert = [];

        for (const element of retreivements) {
            const retreiveElt = await db.collection('retreivements')
                .findOne({ lifeTicketNumber: element.lifeTicketNumber });
            if (!retreiveElt) {
                retreivementsToInsert.push(element);
            }
        }
        console.log('retreivementsToInsert : ', retreivementsToInsert.length);
        if (retreivementsToInsert.length !== 0) {
            const retreivementsSnapshot = await db.collection('retreivements').insertMany(retreivementsToInsert);
            console.log('Insert result : ',retreivementsSnapshot.result);
        }
    } catch (error) {
        console.log(`We have an error : `, error);
    }
});


getProduct = (code) => {
    code = code.slice(0, 3);
    const mapping = {
        'ROB': {
            label: 'ROBUST',
            img: 'assets/imgs/item-robust.png'
        },
        'MIX': {
            label: 'MULTIX',
            img: 'assets/imgs/item-multix.png'
        },
        'SUB': {
            label: 'SUBLIM',
            img: 'assets/imgs/item-sublim.png'
        },
        'HYD': {
            label: 'HYDRO',
            img: 'assets/imgs/item-hydro.png'
        }
    }
    const result = mapping[code] ? mapping[code] : {};
    return result;
}

getRetreivementItems = (data) => {
    return {
        qtyShipped: data.QuantityShipped,
        qtyOrdered: data.QuantityOrdered,
        item: getProduct(data.ItemNumber)
    }
}

app.listen(3128);