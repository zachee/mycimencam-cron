
 // index.js
 const cron = require("node-cron");
 const express = require("express");
 const fs = require("fs");
const moment = require('moment');
const { MongoClient } = require('mongodb');

app = express();

 // schedule tasks to be run on the server 

cron.schedule("*/10 * * * * *", async () => {           // "* * */3 * * *"
    console.log("running a task every 10 sec");
    const fs = require('fs')
    let fileTest = fs.readFileSync(__dirname + '/LIFE_MOCK_DATA.json')
    let jdeResponse = JSON.parse(fileTest)
    let retreivements = [];
    jdeResponse.cpLifeDispatchDetailsReply.forEach(element => {
        let retreiveElt = {
            driverName: element.Header.DriverName,
            registrationLicenseNumber: element.Header.RegistrationLicenseNumber.replace(/\s/g, ''),
            primaryVehicleId: element.Header.PrimaryVehicleId.replace(/\s/g, ''),
            shipTo: parseInt(element.Header.ShipTo),
            soldTo: parseInt(element.Header.SoldTo),
            salesOrderNumber: parseInt(element.Header.SalesOrderNumber),
            shipmentNumber: parseInt(element.Header.ShipmentNumber),
            loadNumber: parseInt(element.Header.LoadNumber),
            actualShipDate: moment(element.Header.ActualShipDate).valueOf(),
            actualShipTime: element.Header.ActualShipTime,
            lifeTicketNumber: parseInt(element.Header.LIFETicketNumber),
            details: getRetreivementItems(element.Details)
        };
        retreivements.push(retreiveElt);
    });
    console.log('retreivements : ', retreivements.length);

    // const mongoDBURL = `mongodb+srv://mycimencam:ohg3YmGw2agRNpUh@cluster0-gt1w5.gcp.mongodb.net/mycimencam?retryWrites=true`; // staging
    // const mongoDBURL = `mongodb+srv://mycimencam:ikryx7BpkNifHNjQ@cluster0-mnzqj.gcp.mongodb.net/mycimencam?retryWrites=true`; // prod
    const mongoDBURL = `mongodb://127.0.0.1:27017/mycimencam`;
    const connection = await MongoClient.connect(mongoDBURL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });

    console.log('----------------------------------------');
    console.log('INSERT NEW Retreivements');
    console.log('----------------------------------------');

    const db = await connection.db();
    const retreivementsSnapshot = await db.collection('retreivements').insertMany(retreivements);
    console.log(retreivementsSnapshot.result);
});


getProduct = (code) => {
    code =code.slice(0,3);
    const mapping = {
        'ROB': {
            label: 'ROBUST',
            img: 'assets/imgs/item-robust.png'
        },
        'MIX': {
            label: 'MULTIX',
            img: 'assets/imgs/item-multix.png'
        },
        'SUB': {
            label: 'SUBLIM',
            img: 'assets/imgs/item-sublim.png'
        },
        'HYD': {
            label: 'HYDRO',
            img: 'assets/imgs/item-hydro.png'
        }
    }
    const result = mapping[code] ? mapping[code] : {};
    return result;
}

getRetreivementItems = (data) => {
    return {
        qtyShipped: data.QuantityShipped,
        qtyOrdered: data.QuantityOrdered,
        item: getProduct(data.ItemNumber)
    }
}

app.listen(3128);